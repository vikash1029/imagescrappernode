const puppeteer = require('puppeteer');

const run = async (search) => {
	
    const GOOGLE_URL = `http://www.google.com/search?hl=en&tbo=d&site=&source=hp&q=${encodeURIComponent(search)}`;
    
    const browser = await puppeteer.launch({ 
    	args: ['--no-sandbox'], 
    	headless: false, 
    });

    const page = await browser.newPage();

    page.setDefaultTimeout(0);

    await page.setUserAgent('Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36');
   	await page.setViewport({ width: 1200, height: 800 });
    
    const navigationPromise = page.waitForNavigation({ waitUntil: "domcontentloaded" }) ;
    
    await page.goto(GOOGLE_URL);
    await navigationPromise;

    
	if (await page.$('.z3dsh') !== null) 
	{
	    await page.click('span[class="z3dsh"]');
  	} 
  	else 
  	{
		await browser.close();
	    throw "There is not any see photos link";
  	}
    
    await page.waitForNavigation({ waitUntil: 'networkidle0' });
   
    let foodImg =  await restaurantImage("Food & drink", page);
    
    let ownerImg = await restaurantImage("By owner", page);
    
	let imagesLink = {
		'foodImageLinks': foodImg,
		'ownerImageLinks': ownerImg 
	};

	await browser.close();
	return imagesLink;
};


const restaurantImage = async (category, page) => {
	try
	{	
		await page.$$eval('div.spF7nQ9kj1t__label.gm2-button-alt', async(selectors, category) => {
			let isCategoryFound = false;
			selectors.forEach(async(selector) => {
				if(selector.textContent == category)
				{
					isCategoryFound = true;
					await selector.click();
				}
			});

			if(isCategoryFound == false)
			{
				throw  category + " not found ";
			}
		}, category);	
		
		await page.waitForNavigation({ waitUntil: 'networkidle0' });

		await page.waitForSelector('div[class=\'gallery-image-low-res\']');

		await page.click('div[class=\'gallery-image-low-res\']');

		const totalImgs = (await page.$$('div.gallery-image-low-res')).length;
		
		if(totalImgs > 1)
		{
			for(i=0; i <= 2; i++)
			{
				await page.keyboard.press("Tab");
				await page.keyboard.type(String.fromCharCode(13));
			}			
		}
		
		try 
		{
			await page.waitForFunction('document.querySelectorAll(".gallery-image-high-res")[2].classList.contains("loaded")', { timeout: 30000 });
		} 
		catch (err) 
		{
			// console.log('No Data Found With Http Image Link');
		}
		
		await delay(4000);

		const imgs = await page.evaluate(() => {
			try 
			{
				const galleryImageSelectors = document.querySelectorAll('.gallery-image-high-res');
				const food = [];
				galleryImageSelectors.forEach(galleryImageSelector => {
					const background_image = window.getComputedStyle(galleryImageSelector).backgroundImage.slice(5, -2);
					if(background_image != 'none')
					{
						food.push(background_image);            	
					} 
				});
				return food.slice(0, 3);
			} catch (err) {
				throw  "Cannot fetch large image after clicking on thumbnail";
			}
		});
		return imgs;
	}
	catch(err)
	{
		let errorMessage = {
			'error': err.message
		};
		return errorMessage;
	}
};

const delay = function (time) {
   return new Promise(function(resolve) { 
       setTimeout(resolve, time)
   });
}

const imageScrapper = function (search) {
    return run(search).catch(function (err) {
        throw err;
    }) 
} 

exports.imageScrapper = imageScrapper;

