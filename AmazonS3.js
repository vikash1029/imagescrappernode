const AWS = require('aws-sdk');
const fs = require('fs');
const Config = require('./Config.js');
const path = require('path');

AWS.config.update({
    region: Config.amazonS3.region,
    accessKeyId: Config.amazonS3.accessKey,
    secretAccessKey: Config.amazonS3.secretAccessKey
});

const s3Bucket = new AWS.S3({ params: {Bucket: Config.amazonS3.imageBucket}})

const imageUpload = async (filePath, folderName) => {
    let fileContent = fs.readFileSync(filePath);
    let fileName = path.basename(filePath);
    let amazonPath = folderName + '/' + fileName;

    let data = {
        Key: amazonPath,
        Body: fileContent,
        ACL: Config.amazonS3.acl
    };
    return new Promise((resolve, reject) => {
        s3Bucket.putObject(data, (err) => {
            if(err) {
                reject(err);
            }
            else {
                resolve({
                    folderName,
                    fileName,
                    filePath
                });
            }
        });
    });
};

exports.imageUpload = imageUpload;
