const fs = require('fs');
const https = require('https');


const saveImageToDisk = function (url, localPath, imageName, callback) {
  let fullPath = localPath + imageName;
  let file = fs.createWriteStream(fullPath);
  
  https.get(url, function(response) {
    response.pipe(file);
    callback(imageName);
  });

}

const downloadImage = function (url, localPath, imageName, callback) {
  return saveImageToDisk(url, localPath, imageName, callback);
} 


exports.downloadImage = downloadImage;