const dotEnv = require('dotenv');
dotEnv.config();

module.exports = {
    dbConnection: {
        eat: {
            host: process.env.DB_HOSTNAME,
            user: process.env.DB_USERNAME,
            password: process.env.DB_PASSWORD,
            database: process.env.DB_DATABASE_EAT
        },
        eatAutomatedCollection: {
            host: process.env.DB_HOSTNAME,
            user: process.env.DB_USERNAME,
            password: process.env.DB_PASSWORD,
            database: process.env.DB_DATABASE_EAT_AUTOMATED_COLLECTION
        }
    },
    amazonS3: {
        baseUrl: process.env.AMAZON_S3_BASEURL,
        accessKey: process.env.AMAZON_KEY,
        secretAccessKey: process.env.AMAZON_SECRET,
        region: process.env.AMAZON_REGION,
        imageBucket: process.env.AMAZON_IMAGES_BUCKET,
        acl: process.env.AMAZON_ACL
    }
    
}