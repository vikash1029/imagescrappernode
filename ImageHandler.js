const fs = require('fs');
const { v4 : uuidv4 }  =  require('uuid');


const createNewFolderForAdvertisementImage = function() {
    let uniqueName = uuidv4().replace(/-/g, "");

    let folderPath = __dirname + '/ImageTemp/' + uniqueName;
    
    fs.mkdir(folderPath, { recursive: true }, function(err) {
        if (err) {
          console.log(err)
        } 
    })
    return uniqueName;
}


const createNewFolder = function () {
    return createNewFolderForAdvertisementImage();
  } 
  
  
  exports.createNewFolder = createNewFolder;
