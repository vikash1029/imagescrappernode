const mysql = require('mysql');
const { v4 : uuidv4 }  =  require('uuid');
const sizeOf = require('image-size');
const {imageScrapper} = require('./ImageScrapper.js');
const {downloadImage} = require('./DownloadImage.js');
const {createNewFolder} = require('./ImageHandler.js');
const {imageUpload} = require('./AmazonS3.js');
const Config = require('./Config.js');


const dbConnection = function(dbConfig) {
    return mysql.createConnection(dbConfig);
}
const eatConn = dbConnection(Config.dbConnection.eat);
const eatAutomatedCollectionConn = dbConnection(Config.dbConnection.eatAutomatedCollection);

eatConn.connect(function(err) {
    if(err) throw err;
    let sql = `SELECT advertisement.id, advertisement.title as restaurantName, JSON_VALUE(advertisement.extra, '$.address') as addres FROM eat.advertisement WHERE
    advertisement.id NOT IN (SELECT adv_id FROM eat.advertisement_images) AND advertisement.id NOT IN (SELECT dagensmenuId FROM eat_automated_collection.google_images) LIMIT 1`;
    eatConn.query(sql, [], async function(err, result){
        if(err) throw err;
        let restaurantId = result[0]['id'];
        let restaurantNameAndAddress = result[0]['restaurantName'] + ' ' + result[0]['addres'];
           
        try {
            let restaurantImage = await imageScrapper(restaurantNameAndAddress);
        
            let errorMsg = 'NULL';
            let foodImageLinks = 'NULL';
            let ownerImageLinks = 'NULL';
            let imageLinks = [];

            if(restaurantImage['foodImageLinks']['error'] == undefined)
            {
               foodImageLinks =  JSON.stringify(restaurantImage['foodImageLinks']);
               imageLinks.push(...restaurantImage['foodImageLinks']);
            }
            else 
            {
                errorMsg = errorMsg + restaurantImage['foodImageLinks']['error'];
            }

            if(restaurantImage['ownerImageLinks']['error'] == undefined)
            {
                ownerImageLinks = JSON.stringify(restaurantImage['ownerImageLinks']);
                imageLinks.push(...restaurantImage['ownerImageLinks']);
            }
            else 
            {
                errorMsg = errorMsg + restaurantImage['ownerImageLinks']['error']; 
            }
        
            
            let timestamp = Math.floor(Date.now() / 1000);

            let imageNames = [];
            let isImageDownloaded = function(imageName) {
                imageNames.push(imageName);
            }
            let folderName = createNewFolder();
            let path = __dirname + '/ImageTemp/' + folderName + '/';

            let removeDuplicateLinks = removeDuplicates(imageLinks);
    
            removeDuplicateLinks.forEach(removeDuplicateLink => {
                let imageName = uuidv4().replace(/-/g, "") + '.png';
                downloadImage(removeDuplicateLink, path, imageName, isImageDownloaded);
            });
            
            await delay(10000);
            var isPrimary = 1;
            imageNames.forEach(imageName => {
                let imagePath = path + imageName;
                imageUpload(imagePath, folderName).then(resp => {
                    buildRestaurantImage(eatConn, restaurantId, resp.fileName, resp.folderName, isPrimary, timestamp, resp.filePath);
                    isPrimary = 0;    
                }).catch(err => {
                   console.log(err);
                });


            });

            eatAutomatedCollectionConn.connect(function(err) {
                if(err) throw err;
                let sql = "INSERT INTO `google_images`(`dagensmenuId`, `foodImages`, `ownerImages`, `createdOn`,`googleImageError`) VALUES (?, ?, ?, ?, ?)";
                eatAutomatedCollectionConn.query(sql, [restaurantId, foodImageLinks, ownerImageLinks, timestamp, errorMsg], function (err, result) {
                    if(err) throw err;
                });
            });
        
        } catch(err) 
        {
            let sql = "INSERT INTO `google_images`(`dagensmenuId`, `createdOn`,`googleImageError`) VALUES (?, ?, ?)";
            eatAutomatedCollectionConn.query(sql, [restaurantId, timestamp, err.message, ], function(err, result) {
                if (err) throw err;
                process.exit();
            });
        }
    });
});

const buildRestaurantImage = function (eatConn, restaurantId, imageName, folderName, isPrimary, timestamp, imagePath) {
        sizeOf(imagePath, function (err, dimensions) {
            let imageHeight = dimensions.height;
            let imageWidth = dimensions.width;
            let sql = "INSERT INTO `advertisement_images` (`adv_id`, `image_name`, `image_folder`, `is_Primary_Image`, `creation_date`, `width`, `height`) VALUES (?, ?, ?, ?, ?, ?, ?)";
            eatConn.query(sql, [restaurantId, imageName, folderName, isPrimary, timestamp, imageWidth, imageHeight], function (err, result) {
                if(err) throw err;
            });
        });
}

const delay = function (time) {
    return new Promise(function(resolve) { 
        setTimeout(resolve, time)
    });
 }

const removeDuplicates = function (imageLinks) {
    let unique = {};
    imageLinks.forEach(function(i) {
        if(!unique[i]) 
        {
            unique[i] = true;
        }
    });
    return Object.keys(unique);
}